import logging

import discord
from discord.ext import commands
from discord.ext.commands import Context

from ZhySu import ZhySuBot
from ZhySu.Utils import Messages


class Mastery(commands.Cog):

    def __init__(self, bot: ZhySuBot):
        self.bot = bot

    @commands.command(
        name='stop',
        hidden=True
    )
    @commands.is_owner()
    async def _stop(self, ctx: Context):
        """Stops the bot."""
        logging.getLogger(__name__).info('{0.author.name}#{0.author.discriminator} wants to stop the bot.'.format(ctx))
        try:
            await self.bot.logout()
        except Exception as e:
            await ctx.send(embed=discord.Embed(
                title=Messages.get('mastery.error.stop', None),
                description='{0}: {1}'.format(type(e).__name__, e),
                color=discord.Color.dark_red()
            ).set_footer(text=Messages.get('global.requested', None).format(ctx), icon_url=ctx.author.avatar_url))
            logging.getLogger(__name__).error('Failed to stop the bot:')
            logging.getLogger(__name__).exception(e)

    @commands.command(
        name='load',
        hidden=True
    )
    @commands.is_owner()
    async def _load(self, ctx: Context, *, module: str):
        """Loads a module."""
        logging.getLogger(__name__).info('{0.author.name}#{0.author.discriminator} wants to load module "{1}"'.format(ctx, module))
        try:
            self.bot.load_extension(module)
        except Exception as e:
            await ctx.send(embed=discord.Embed(
                title=Messages.get('mastery.error.load', None),
                description='{0}: {1}'.format(type(e).__name__, e),
                color=discord.Color.dark_red()
            ).set_footer(text=Messages.get('global.requested', None).format(ctx), icon_url=ctx.author.avatar_url))
            logging.getLogger(__name__).error('Failed to load module "{0}"'.format(module))
            logging.getLogger(__name__).exception(e)
        else:
            await ctx.send(embed=discord.Embed(
                title=Messages.get('mastery.success.load.title', None),
                description=Messages.get('mastery.success.load.description', None).format(module),
                color=discord.Color.green()
            ).set_footer(text=Messages.get('global.requested', None).format(ctx), icon_url=ctx.author.avatar_url))

    @commands.command(
        name='unload',
        hidden=True
    )
    @commands.is_owner()
    async def _unload(self, ctx: Context, *, module: str):
        """Unloads a module."""
        logging.getLogger(__name__).info('{0.author.name}#{0.author.discriminator} wants to unload module "{1}"'.format(ctx, module))
        try:
            self.bot.unload_extension(module)
        except Exception as e:
            await ctx.send(embed=discord.Embed(
                title=Messages.get('mastery.error.unload', None),
                description='{0}: {1}'.format(type(e).__name__, e),
                color=discord.Color.dark_red()
            ).set_footer(text=Messages.get('global.requested', None).format(ctx), icon_url=ctx.author.avatar_url))
            logging.getLogger(__name__).error('Failed to unload module "{0}"'.format(module))
            logging.getLogger(__name__).exception(e)
        else:
            await ctx.send(embed=discord.Embed(
                title=Messages.get('mastery.success.unload.title', None),
                description=Messages.get('mastery.success.unload.description', None).format(module),
                color=discord.Color.green()
            ).set_footer(text=Messages.get('global.requested', None).format(ctx), icon_url=ctx.author.avatar_url))

    @commands.command(
        name='reload',
        hidden=True
    )
    @commands.is_owner()
    async def _reload(self, ctx: Context, *, module: str):
        """Reloads a module."""
        logging.getLogger(__name__).info('{0.author.name}#{0.author.discriminator} wants to reload module "{1}"'.format(ctx, module))
        try:
            self.bot.unload_extension(module)
            self.bot.load_extension(module)
        except Exception as e:
            await ctx.send(embed=discord.Embed(
                title=Messages.get('mastery.error.reload', None),
                description='{0}: {1}'.format(type(e).__name__, e),
                color=discord.Color.dark_red()
            ).set_footer(text=Messages.get('global.requested', None).format(ctx), icon_url=ctx.author.avatar_url))
            logging.getLogger(__name__).error('Failed to reload module "{0}"'.format(module))
            logging.getLogger(__name__).exception(e)
        else:
            await ctx.send(embed=discord.Embed(
                title=Messages.get('mastery.success.reload.title', None),
                description=Messages.get('mastery.success.reload.description', None).format(module),
                color=discord.Color.green()
            ).set_footer(text=Messages.get('global.requested', None).format(ctx), icon_url=ctx.author.avatar_url))


def setup(bot: ZhySuBot):
    bot.add_cog(Mastery(bot))
    logging.getLogger(__name__).info('Extension enabled')


def teardown(bot: ZhySuBot):
    bot.remove_cog('Mastery')
    logging.getLogger(__name__).info('Extension disabled')
