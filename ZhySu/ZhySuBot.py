import logging
import traceback

import discord
import emoji
from discord.ext.commands import AutoShardedBot, Context, CommandError, CommandOnCooldown, MissingPermissions

from ZhySu.Utils import Configuration, Messages


class ZhySuBot(AutoShardedBot):

    def __init__(self):
        # Start Bot
        super().__init__(
            command_prefix=Configuration().get('bot.prefix')
        )

        # Remove default help-Command
        self.remove_command('help')

        # Autoload Extensions
        for ext in Configuration().get('extensions.autoload'):
            self.load_extension(ext)

    async def on_ready(self):
        logging.getLogger(__name__).info('Logged on as {0}!'.format(self.user))
        await self.change_presence(
            status=discord.Status.online,
            activity=discord.Game(emoji.emojize(Messages.get('zhysubot.presence', None)))
        )

    async def on_message(self, message: discord.Message):
        if self.user.mentioned_in(message) and message.mention_everyone is False:
            await message.add_reaction(emoji.emojize(':waving_hand:'))
        await self.process_commands(message)

    async def on_command_error(self, ctx: Context, error: CommandError):
        if isinstance(error, CommandOnCooldown):
            retry_after = round(error.retry_after)
            await ctx.send(embed=discord.Embed(
                title=Messages.get('zhysubot.error.cooldown.title', ctx.guild),
                description=Messages.get('zhysubot.error.cooldown.description', ctx.guild).format(retry_after),
                color=discord.Color.dark_red()
            ))
            logging.getLogger(__name__).info(
                '{0.author.name}#{0.author.discriminator} tried to use "{0.message.content}" but the command is still in cooldown for {1} seconds.'.format(
                    ctx, retry_after
                )
            )
        elif isinstance(error, MissingPermissions):
            missing_perms = ', '.join(error.missing_perms)
            await ctx.send(embed=discord.Embed(
                title=Messages.get('zhysubot.error.missing_perms.title', ctx.guild),
                description=Messages.get('zhysubot.error.missing_perms.description', ctx.guild).format(missing_perms),
                color=discord.Color.dark_red()
            ))
            logging.getLogger(__name__).info(
                '{0.author.name}#{0.author.discriminator} tried to use "{0.message.content}" but he did not have the following permissions: {0}.'.format(
                    ctx, missing_perms
                )
            )
        else:
            app_info = await self.application_info()

            if ctx.guild is not None:
                creator = ctx.guild.get_member(app_info.owner.id)
            else:
                creator = None

            if creator is not None:
                contact = creator.mention
            else:
                contact = '{0}#{1}'.format(app_info.owner.name, app_info.owner.discriminator)

            await ctx.send(embed=discord.Embed(
                title=Messages.get('zhysubot.error.unknown.title', ctx.guild),
                description=Messages.get('zhysubot.error.unknown.description', ctx.guild).format(contact),
                color=discord.Color.dark_red()
            ))
            logging.getLogger(__name__).warning(
                '{0.author.name}#{0.author.discriminator} tried to use "{0.message.content}" but the command failed'.format(ctx)
            )
            logging.getLogger(__name__).error('{0}: {1}\n{2}'.format(
                    type(error).__name__,
                    error,
                    ''.join(traceback.format_exception(type(error), error, error.__traceback__))
            ))
