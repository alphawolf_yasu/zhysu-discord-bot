import yaml

from ZhySu.Utils.Metaclasses import Singleton


class ConfigKeyNotFoundException(Exception):
    """Gets raised if the given key cannot be found within the configuration-file"""
    pass


class Configuration(metaclass=Singleton):
    __config = {}

    def __init__(self):
        with open('config.yaml', 'r', encoding='utf-8') as configFile:
            self.__config = yaml.safe_load(configFile)

    def get(self, key: str):
        tree = key.split('.')
        fork = self.__config

        for twig in tree:
            if fork is not None and twig in fork:
                fork = fork[twig]
            else:
                raise ConfigKeyNotFoundException('Entry for path "{0}" could not be found in config.yaml'.format(key))

        return fork

    def exists(self, key: str):
        try:
            return self.get(key) is not None
        except ConfigKeyNotFoundException:
            return False
